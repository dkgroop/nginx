FROM nginx

RUN echo "daemon off;" >> /etc/nginx/nginx.conf

COPY default.conf /etc/nginx/conf.d/default.conf

CMD service nginx start
